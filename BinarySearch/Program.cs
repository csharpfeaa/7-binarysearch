﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearch
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numere = { 3, 2, 1, 5, 6, 78, 27, 10 };

            Console.WriteLine("Vector initial");
            AfiseazaVector(numere);

            Console.WriteLine("Vector sortat");
            int[] numereSortate = SorteazaVector(numere);
            AfiseazaVector(numere);

            // cauta o valoare din vector
            Console.WriteLine();
            CautaValoare(numereSortate, 10);

            Console.WriteLine();
            CautaValoare(numereSortate, 13);

            Console.WriteLine();
            CautaValoare(numereSortate, 7);

            Console.WriteLine();
            CautaValoare(numereSortate, 78);
            

            Console.ReadKey();
        }

        static int[] SorteazaVector(int[] v)
        {
            for (int i = 0; i < v.Length - 1; i++)
            {
                for (int j = 0; j < v.Length - i - 1; j++)
                {
                    if (v[j] > v[j + 1])
                    {
                        int temp = v[j + 1];
                        v[j + 1] = v[j];
                        v[j] = temp;
                    }
                }
            }

            return v;
        }

        static void CautaValoare(int[] v, int nr)
        {
            int limInf = 0;
            int limSup = v.Length - 1;
            int index = -1;

            while (limInf <= limSup)
            {
                int mijloc = (limInf + limSup) / 2;

                Console.WriteLine("inf: {0}, sup: {1}, mij: {2}", limInf, limSup, mijloc);

                if (v[mijloc] == nr)
                {
                    index = mijloc;
                    break;
                }

                if (v[mijloc] > nr)
                {
                    limSup = mijloc - 1;
                }
                else if (v[mijloc] < nr)
                {
                    limInf = mijloc + 1;
                }
            }

            if (index >= 0)
                Console.WriteLine("Numarul cautat: {0}, EXISTA in vector, la pozitia {1}", nr, index + 1);
            else
                Console.WriteLine("Numarul cautat: {0}, NU exista in vector.", nr);
        }

        static void AfiseazaVector(int[] v)
        {
            for (int i = 0; i < v.Length; i++)
            {
                Console.Write(v[i]);

                if (i < v.Length - 1)
                    Console.Write(", ");
            }

            Console.WriteLine();
        }
    }
}
